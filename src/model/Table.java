package model;

import java.util.ArrayList;
import java.util.List;


public class Table {
    private List<Cube> cubesSurTable = new ArrayList<Cube> ();

    public void poseSurTable(Cube cube) {
    	ajouterSurLaListe(cube);
    }

    public void ajouterSurLaListe(Cube cube) {
    	cubesSurTable.add(cube);
    }

    public boolean poseSurCube(Cube cubeTenu,int numero) {
    	boolean rep = false;
    	Cube cubeCible = trouverCube(numero);
    	if (cubeCible!=null) {
    		if (cubeCible.estCeQueJePeuxMePoserSurToi(cubeTenu)) {
    			ajouterSurCube(cubeTenu, cubeCible);
        		rep=true;
			}
    		else {
        		System.err.println("Impossible de posser sur un plus petit cube");
        	}
		}else {
    		System.err.println("Cube non trouv� ou impossible de le posser dessus");
		}
    	return rep;
    }
    
    public Cube trouverCube(int numero) {
		// TODO Auto-generated method stub
    	Cube res=null;
    	boolean flag=true;
    	
        for(int i=0;i<cubesSurTable.size() && flag;i++) {
        	if(cubesSurTable.get(i).correspondTu(numero)) {
        		res=cubesSurTable.get(i);
        		flag=false;
        	}
        }
        if (res==null) {
    		System.err.println("Cube non trouv� ou il est en dessus");
        }
        return res;
	}
    
    
    public Cube prendsCube(int numero) {
		Cube res = trouverCube(numero);
		supprimerCube(res);
		return res;
	}

	
    

    public void ajouterSurCube(Cube cubeTenu, Cube cubeCible) {
    	cubeTenu.setDessous(cubeCible);
    	for (int i=0;i< cubesSurTable.size();i++)
        {
        	if(cubesSurTable.get(i).correspondTu(cubeCible)) {
        		cubesSurTable.set(i,cubeTenu);
        	}
        }
    }

    public void supprimerCube(Cube cube) {
        for (int i=0;i< cubesSurTable.size();i++)
        {
        	if(cubesSurTable.get(i).equals(cube)) {
        		if(cube.getDessous()==null) {
        			cubesSurTable.remove(i);
        		}
        		else {
            		cubesSurTable.set(i, cube.getDessous());
        		}
        		
        		cube.setDessous(null);
        	}
        }
    }

    public List<Cube> getCubesSurTable() {
    	
        return this.cubesSurTable;
    }

    public void setCubesSurTable(List<Cube> value) {

    	this.cubesSurTable = value;
    }
    
    private int howManyLignesInThisCol(Cube c) {
    	int i=0;
    	while(c!= null) {
    		c=c.getDessous();
    		i++;
    	}
		return i;
    	
    }
    private int maxDeLignesDansColonnes() {
    	int compteur = 0;
    	for (int i = 0 ; i< cubesSurTable.size();i++) {
    		if(howManyLignesInThisCol(cubesSurTable.get(i))>compteur) {
    			compteur = howManyLignesInThisCol(cubesSurTable.get(i));
    		}
    		
    	}
		return compteur;	
    }
    public void afficherTable() {
    	int lignes = maxDeLignesDansColonnes();
    	int colonnes = cubesSurTable.size();
    	Cube[][] table = new Cube[lignes][colonnes];
    	for (int i = 0; i<colonnes; i++) {
				Cube c = cubesSurTable.get(i);
				int j=howManyLignesInThisCol(c)-1;
				while(c!=null) {
					table[j][i]=c;
					c=c.getDessous();
					j--;
				}
			}
    	affichageGrille(table,lignes,colonnes);
    	
			
    }

	private void affichageGrille(Cube[][] table, int lignes, int colonnes) {
		for (int i = lignes-1; i >=0; i--) {
			for (int j = 0; j < colonnes; j++) {
				if (table[i][j]==null) {
					System.out.print("      ");
				}else {
					System.out.print(table[i][j]);
				}
			}
			System.out.println("");
		}
		
	}

	

}
