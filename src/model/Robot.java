package model;

public class Robot {
    private int numero;

    private Cube cubeTenu;

    private Table support;
    
    public Robot(Table support) {
    	this.support=support;
    }
    
    public void creerCube(tailleCube taille, couleurCube couleur) {
    	if (estVide()) {
    		setCubeTenu(new Cube(taille, couleur, getNumeroCube()));
    		incrementeNumCube();
		}
    	else {
    		System.err.println("il existe un cube dans la main du robot");
    	}
    }

    public boolean estVide() {
        return this.cubeTenu==null;
    }

    public void incrementeNumCube() {
    	this.numero++;
    }

    public void poserSurTable() {
    	if (!estVide()) {
	    	support.poseSurTable(getCubeTenu());
	    	setCubeTenu(null);
    	}
    	else {
			System.err.println("il n'y a pas de cube dans la main du robot");
		}
    }



    public void detruireCube() {
    	if (!estVide()) {
    		setCubeTenu(null);
		}
    	else {
    		System.err.println("Il n'y a pas de cube dans la main du robot");
		}
    }

    public void prendreCube(int numero) {
    	if (estVide()) {
			setCubeTenu(support.prendsCube(numero));
		}
    	else {
    		System.err.println("La main n'est pas vide");
    	}
    	
    }

    public int getNumeroCube() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.numero;
    }

    public void setNumeroCube(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.numero = value;
    }

    public Cube getCubeTenu() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.cubeTenu;
    }

    public void setCubeTenu(Cube value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.cubeTenu = value;
    }

	public void afficherRobot() {
		if (estVide()) {
			System.out.println("   ---- :[ ]");
//			System.out.println(" /");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println("___");
		}
    	else {
    		System.out.println("   ----"+getCubeTenu().toString());
//    		System.out.println(" /");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println(" |");
//    		System.out.println("___");
    	}
		
	}

	public void poserSurCube(int numero) {
		if (!estVide()) {
    		if(support.poseSurCube(cubeTenu, numero)) {
        		setCubeTenu(null);
    		}
		}
    	else {
			System.err.println("Il n'y a pas de cube dans la main du robot");
		}
	}

}
