package model;

import in.keyboard.Keyboard;

public class Monde {
	
	public static void ecrire(couleurCube couleur, tailleCube taille) {
		System.out.println(taille.toString().charAt(0) + couleur.toString().charAt(0));
	}
	
	public static void main(String[] args) {
		Table t = new Table();
		Robot r = new Robot(t);
		t.afficherTable();
//		r.creerCube(tailleCube.grand, couleurCube.rouge);
//		r.afficherRobot();
//		r.detruireCube();
//		r.afficherRobot();
		tailleCube taille;
		couleurCube couleur;
		int numero;
		char c;
			do {
			System.out.println("1 - creer un cube");
			System.out.println("2 - d�truire un cube");
			System.out.println("3 - prendre un cube");
			System.out.println("4 - poser cube sur table");
			System.out.println("5 - poser cube sur un cube");
			System.out.println("f - fin du monde");
			System.out.print("Votre choix : ");
			c = Keyboard.getChar();
			switch (c) {
			case '1': 
				System.out.print("Couleur du cube : ");
				switch (Keyboard.getString()) {
				case "rouge":
						couleur = couleurCube.rouge;
					break;
				case "vert":
					couleur = couleurCube.vert;
					break;
				case "bleu":
					couleur = couleurCube.bleu;
					break;

				default:
					couleur = couleurCube.rouge;
					break;
				}
				System.out.print("Taille (grand/moyen/petit) : ");
				switch (Keyboard.getString()) {
				case "grand":
					taille = tailleCube.grand;
					break;
				case "moyen":
					taille = tailleCube.moyen;
					break;
				case "petit":
					taille = tailleCube.petit;
					break;

				default:
					taille = tailleCube.petit;
					break;
				}
				r.creerCube(taille, couleur);
//				System.out.println(r.getCubeTenu().toString());
				break;
			case '2'://destruction du cube tenu par le robot
				r.detruireCube();
				break;
			case '3'://prendre un cube
				System.out.print("Numero du cube : ");
				numero = Keyboard.getInt();
				r.prendreCube(numero);
				break;
			case '4': //poser un cube sur table
				r.poserSurTable();
				break;
			case '5': //poser un cube sur un cube
				System.out.print("Numero du cube : ");
				numero = Keyboard.getInt();
				r.poserSurCube(numero);
				break;
			default:
				break;
			}
			afficherMonde(r, t);
			} while (c != 'f');
		}

		private static void afficherMonde(Robot r, Table t) {
			r.afficherRobot();
			t.afficherTable();
//			Keyboard.pause();
		
		}
		
	}
