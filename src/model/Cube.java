package model;

public class Cube {
    private tailleCube taille;

    private couleurCube couleur;

    private int numero;

    private Cube dessous;
    
    public Cube(tailleCube taille,couleurCube couleur,int numero) {
    	this.taille=taille;
    	this.couleur=couleur;
    	this.numero=numero;
    	
    }
    
    public void setTaille(tailleCube value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.taille = value;
    }

    public void setCouleur(couleurCube value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.couleur = value;
    }

    public int getNumero() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.numero;
    }

    public void setNumero(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.numero = value;
    }

    public boolean correspondTu(tailleCube taille, couleurCube couleur) {
        // TODO Auto-generated return
        return (this.couleur.equals(couleur)&&this.taille.equals(taille));
    }

    public couleurCube getCouleur() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.couleur;
    }

    public tailleCube getTaille() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.taille;
    }

    public boolean correspondTu(int numero) {
        // TODO Auto-generated return
        return (this.numero == numero);
    }

    public Cube getDessous() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.dessous;
    }

    public void setDessous(Cube value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.dessous = value;
    }
    public boolean estCeQueJePeuxMePoserSurToi(Cube c) {
    	boolean rep = false;
    		if(this.getTaille()==tailleCube.grand) {
    			rep = true;
    		}
    		else if (this.getTaille()==tailleCube.moyen) {
    			if(c.getTaille()==tailleCube.moyen||c.getTaille()==tailleCube.petit) {
    				rep=true;
    			}
			}
    		else {
    			if(c.getTaille()==tailleCube.petit) {
    				rep=true;
    			}
			}
    		return rep;
    	
    }
    @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	
    	return "["+getTaille().toString().toUpperCase().charAt(0)+getCouleur().toString().toUpperCase().charAt(0)+String.format("%02d",getNumero())+"]";
    }

	public boolean correspondTu(Cube cubeCible) {
		// TODO Auto-generated method stub
		return (this.couleur.equals(cubeCible.couleur)&&this.taille.equals(cubeCible.taille)&&this.numero==cubeCible.numero);
	}
}
